/*
Одно из основных преимуществ программирование - переиспользование кода.
Это может быть переиспользование своего, так и чужого кода. Для использования
чужого кода в Node js используется система модулей.
Модули можно поделить на встроенные Node js модули (вот, например, https://nodejs.org/api/perf_hooks.html)
и модули NPM (это те, что другие разработчики создают сами, вот мой, например, https://www.npmjs.com/package/@vl-utils/log)
*/
// Пример подключения одного из модулей Node js
const perfHooks = require('perf_hooks');
const performance = perfHooks.performance

// Получаем текущее время
let timeFirst = performance.now()

// Выполняем некую долгую операцию на 3*10^6 операций
let str = 'a'
for (let i = 0; i < 3e6; i++) {
  if (str.length > 255) {
    str = 'b'
  } else {
    str += 'b'
  }
}

// Выводим в консоль потраченное время
console.log(performance.now() - timeFirst)


// 5. С помощью встроенного модуля, преобразовать параметры запроса в элементы запроса
// Пример:
// https://www.domain.org/home?path=grand&path2=hotel
// ->
// https://www.domain.org/home/grand/hotel
function modules(urlString) {
  let resultURL = new URL(urlString)
  resultURL.searchParams.forEach(function(value, name,searchParams) {
	resultURL.pathname += '/' + value
  })
  resultURL.search = ''
  return resultURL.href
}

console.log(modules('https://www.domain.org/home?path=grand&path2=hotel'))


// Сторонние модули
// Одно из мощных преимуществ node js над другими фреймворками - наличие богатой
// библиотеки модулей. Основной репозиторий, который чаще всего используется для
// хранения подобных модулей https://www.npmjs.com/

// 6. Найти библиотеку lodash, установить ее. Используя ее методы, превратить
// массив с массивами пар в объект
// Пример:
// [['a', 1], ['b', 2]]
// ->
// { 'a': 1, 'b': 2 }
function modulesSecond(array) {
  return 'result'
}

console.log(modulesSecond( [
  ['userName', 'Жужа'],
  ['typeAction', 'Танец']
] ))
