// Задачки на типы

// 1. Массивы
// Используя цикл for перебрать элементы массива arr и создать массив types с типами элементов
function typesFirst(arr) {
	var types = [];
	for ( let i = 0; i < arr.length; i++ ) {
		types.push(typeof(arr[i]));
	}
	return types;
}


// Результат
console.log(
  typesFirst([
    1,
    'magic',
    2,
    'true',
    true,
    3,
    '3',
    { '3': 4 },
    4,
    [5,6,7],
    7
  ])
)


// 2. Строки
// Разбить строку на слова, перевернуть каждое слово и собрать строку обратно
// Пример:  Мама мыла раму и читала книгу -> амаМ алым умар и алатич угинк
function typesSecond(str) {
	var words = str.split(' ')
	let reversed = ''
	words.forEach(function callbackFn(element, index){
		let reversedStr = Array.from(element).reverse();
		var temp = ''
		temp = reversedStr.toString()
		reversed = reversed + temp + ' ';
		reversed = reversed.replaceAll(',','')
	})
	return reversed
}


// Результат
console.log(
  typesSecond('Корабли лавировали между скал залива Венеции')
)


// 3. Объекты
// На вход получаешь объект с двумя полями. Одно поле - пользователь, второе - действие.
// Необходимо удалить из объекта пользователя пароль, а в объект action добавить поле code.
// Коды действия заданы в константе ACTIONS. На выход отдать объект с полями user и action
function typesThird(obj) {
	const ACTIONS = {
		'del': '1755',
		'add': '9458',
		'upd': '3319'
	}
	let newObj = { }
	for ( const socket in obj ) {
		newObj[socket] = { }
		for ( const prop in obj[socket])	{
			if ( prop != 'password' ) {
				newObj[socket][prop] = obj[socket][prop]
			}
			if ( socket == 'action' ) {
				if ( prop == 'type' ) {
					newObj.action.code = ACTIONS[obj.action[prop]]
				}
			}
			console.log(`${prop} : ${obj[socket][prop]}`)
			console.log(`${prop} : ${newObj[socket][prop]}`)
		}
	}	
	return newObj
}


// Результат
console.log(
  typesThird({
    user: {
      name: 'Joseph',
      phone: '8-800-555-35-35',
      password: 'ultra_bezopasniy'
    },
    action: {
      type: 'add'
    }
  })
)

// 4. Функции
// В JS функции тоже можно записать в переменные. Достаточно часто функции передаются в
// функции в качестве аргументов. Иногда такие функции называют коллбэками, но, по сути,
// это обычные функции
// На вход поданы массив строк и две функции - updater и cb. Необходимо пройтись по массиву,
// для каждого значения вызвать updater. На основе результатов updater сформировать массив строк
// разных погод и вызвать cb, передав в него этот массив
function typesFourth(arr, updater, cb) {
	let temp =[]
	arr.forEach(function(element, index){
		temp.push(Object.values(updater(element)))
	})
	cb(temp)
}

// Результат
typesFourth(
  ['rain', 'snow', 'sunny', 'glory'],
  function (weather) {
    let result

    switch (weather) {
      case 'rain':
      case 'snow':
        result = { weather: 'плохая' }
        break
      case 'sunny':
        result = { weather: 'хорошая' }
        break
      default:
        result = { weather: 'неизвестная' }
        break
    }

    return result
  },
  function (weathers) {
    for (let weather of weathers) {
      console.log(`Погода ${weather}`);
    }
  }
)
