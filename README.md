# node-train

Добро пожаловать в проект Node Train

## Установка Node JS
Организация проекта подразумевает использование Node JS - программного комплекса,
способного запускать JavaScript код вне браузера

[Скачать NodeJS](https://github.com/coreybutler/nvm-windows/releases) (Докрутить
до Assets и скачать, например, nvm-setup.zip)

[Инструкция по работе с NVM](https://habr.com/ru/company/timeweb/blog/541452/)
